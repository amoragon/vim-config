set nocompatible
filetype off

" Getting Vundle up and running
set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"Let Vundle manage itself.
Plugin 'gmarik/Vundle.vim'

"Plugins
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'altercation/vim-colors-solarized'
Plugin 'kien/ctrlp.vim'
Plugin 'drmingdrmer/xptemplate'
Plugin 'tmhedberg/matchit'
Plugin 'scrooloose/nerdcommenter'
Plugin 'powerline/powerline', {'rtp': 'powerline/bindings/vim/'}

call vundle#end()

filetype on 
filetype plugin on 
filetype indent on

" Tabstops are 4 spaces
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set softtabstop=4

" syntax highlight on
syntax on

" Set solarized as colorscheme
set background=dark
colorscheme solarized

" Resalta la linea donde esta el cursor
set cursorline

" Con F5 cambiamos el tema de solarized
call togglebg#map("<F5>")

" Fijamos los numeros de linea
set number

" Make the 'cw' and like commands put a $ at the end instead of just deleting
" the text and replacing it
set cpoptions+=ces$

set mps+=<:>

" Enable search highlighting
set hlsearch

" Incrementally match the search
set incsearch

" Activating smartcase searching
set smartcase

" tell VIM to always put a status line in, even if there is only one window
set laststatus=2

" Make the command-line completion better
set wildmenu

" New splits of window on the right
set splitright

" System default for mappings is now the "," character
let mapleader = ","

set encoding=utf-8

set t_Co=256

let g:Powerline_symbols='fancy'

